﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ReservationResort.Data.EF//.metadata
{
    #region Customers

    public class CustomerMetadata
    {
        [StringLength(15, ErrorMessage = "***First Name must be 15 characters or less")]
        [Required(ErrorMessage = "***Required***")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [StringLength(20, ErrorMessage = "***Last Name must be 20characters or less")]
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "***Required***")]
        public string LastName { get; set; }

        [Display(Name = "Profile Picture")]
        public string PhotoURL { get; set; }

        [Display(Name = "Notes")]
        public string SpecialNotes { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}",
            ApplyFormatInEditMode = true)]
        [Display(Name = "Date Added")]
        [Required(ErrorMessage = "***Required***")]
        public System.DateTime DateAdded { get; set; }

        [Required(ErrorMessage = "*** Required")]
        [EmailAddress(ErrorMessage = "Must be a valid email")]
        [StringLength(50, ErrorMessage = "Must be 50 characters of less")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*** Required")]
        [Phone(ErrorMessage = "Must be a valid phone number")]
        [StringLength(15, ErrorMessage = "Must be 15 characters or less")]
        public string Phone { get; set; }
    }

    [MetadataType(typeof(CustomerMetadata))]
    public partial class Customer {
        [Display (Name = "Name")]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

    }

    #endregion

    #region Reservation
    public class ReservationMetadata
    {

        [Key]
        public int ReservationID { get; set; }

        [Key]
        public int ResortLocationID { get; set; }

        [Key]
        public int CustomerID { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}",
           ApplyFormatInEditMode = true)]
        [Display(Name = "Reservation Date")]
        [Required(ErrorMessage = "***Required***")]
        public System.DateTime ReservationDate { get; set; }


    }

    [MetadataType(typeof(ReservationMetadata))]
    public partial class CustomerResortLocation { }

    #endregion

    #region ResortLocation

    public class ResortLocationsMetadata
    {
        [Required(ErrorMessage = "***Required***")]
        [Display(Name = "Resort Name")]
        [StringLength(50, ErrorMessage = "Resort Name must be 50 characters or less")]
        public string ResortName { get; set; }

        [StringLength(100, ErrorMessage = "Address must be 100 characters or less")]
        [Required(ErrorMessage = "***Required***")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessage = "City must be 50 characters or less")]
        [Required(ErrorMessage = "***Required***")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage ="State must be 2 characters")]
        [Required(ErrorMessage = "***Required***")]
        public string State { get; set; }

        [Display(Name ="Zip Code")]
        [Required(ErrorMessage = "***Required***")]
        [RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage ="Invalid Zip")]
        public int ZipCode { get; set; }

        [Required(ErrorMessage = "***Required***")]
        [Range(0, 2, ErrorMessage ="Reservation must be between 0, 2")]
        public byte ReservationLimit { get; set; }
    }

    [MetadataType(typeof(ResortLocationsMetadata))]
    public partial class ResortLocation { }

    #endregion
}
