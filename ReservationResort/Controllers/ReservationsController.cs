﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationResort.Data.EF;
using Microsoft.AspNet.Identity;//adding for current user ID

namespace ReservationResort.Controllers
{
    [Authorize]
    public class ReservationsController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: Reservations
        public ActionResult Index()
        {
            //var reservations = db.Reservations.Include(r => r.Customer).Include(r => r.ResortLocation);

            if (User.IsInRole("Admin"))
            {
                List<ReservationResort.Data.EF.Reservation> reservations = db.Reservations.Include(r => r.Customer).Include(r => r.ResortLocation).ToList();
                return View(reservations);
            }
           else if(User.IsInRole("employee"))
            {
                var currentUserID = User.Identity.GetUserId();
                var ReservationsEmployeeLoc =
                    (from R in db.Reservations
                     join RL in db.ResortLocations on R.ResortLocationID equals RL.ResortLocationID
                     join EL in db.EmpLocations on RL.ResortLocationID equals EL.ResortLocationID
                     where EL.UserID == currentUserID
                     select R).ToList();

                return View(ReservationsEmployeeLoc);
                    
            }
            else
            {
                //Who the user is
                var currentUserID = User.Identity.GetUserId();

                //creating list of only this user Reservation items
                List<ReservationResort.Data.EF.Reservation> reservations =
                    db.Reservations.Where(x => x.CustomerID == currentUserID)
                    .Include(r => r.Customer).Include(r => r.ResortLocation)
                    .ToList();

                return View(reservations);
                    
            }
            //return View(reservations.ToList());
        }

        //Creating the Reservations
        public ActionResult customerReservation(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //create a new Reservation
            ReservationResort.Data.EF.Reservation Reservation = new Data.EF.Reservation();
            Reservation.ResortLocationID = (int)id;
            Reservation.ReservationDate = DateTime.Now;
            Reservation.CustomerID = User.Identity.GetUserId();

            db.Reservations.Add(Reservation);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //Updating the Record with the return data

        public ActionResult Return(int id)
        {
            var item = db.Reservations.Find(id);
            item.ReservationDate = DateTime.Now;

            //Save and Update
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();

            var currentUserId = User.Identity.GetUserId();
            //Creating a list of Only this User's Reservation items
            List<ReservationResort.Data.EF.Reservation> reservations =
                db.Reservations.Where(x => x.CustomerID == currentUserId)
                .Include(r => r.Customer).Include(r => r.ResortLocation)
                .ToList();

            return View("Index", reservations);

        }

        // GET: Reservations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // GET: Reservations/Create
        public ActionResult Create()
        {
            var currentUser = User.Identity.GetUserId();
            
                ViewBag.CustomerID = new SelectList(db.Customers.Where(x => x.ID == currentUser), "ID", "FirstName");
                ViewBag.ResortLocationID = new SelectList(db.ResortLocations, "ResortLocationID", "ResortName");
                     
            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReservationID,ResortLocationID,CustomerID,ReservationDate")] Reservation reservation)
        {
           

            //This is going to be the reservation Limit for the Current Location
            var RL = (from rl in db.ResortLocations
                      where rl.ResortLocationID == reservation.ResortLocationID
                      select rl.ReservationLimit).FirstOrDefault();

            //This list is all the Reservation at that location
            var reservationAtLocation = (from res in db.Reservations
                                         where res.ResortLocationID ==
                                         reservation.ResortLocationID
                                         select res).ToList();

            //This will get all reservation on the this location on this date
            var countOnDate = reservationAtLocation.Where(x => x.ReservationDate == reservation.ReservationDate).Count();

            //var nbrOfRes = reservationAtLocation.Where(x => x.ReservationDate == reservation.ReservationDate);
            //var number = nbrOfRes.Count();

            if (RL > countOnDate)
            {
                reservation.CustomerID = User.Identity.GetUserId();

                if (ModelState.IsValid)
                {
                    

                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", reservation.CustomerID);
                ViewBag.ResortLocationID = new SelectList(db.ResortLocations, "ResortLocationID", "ResortName", reservation.ResortLocationID);
                return View(reservation);
            }
            else
            {
                ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", reservation.CustomerID);
                ViewBag.ResortLocationID = new SelectList(db.ResortLocations, "ResortLocationID", "ResortName", reservation.ResortLocationID);
                ViewBag.LocationFullError = "Sorry, This location is full for this date. Please select another location or another date";
                return View(reservation);
            }
            
        }
     

        // GET: Reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", reservation.CustomerID);
            ViewBag.ResortLocationID = new SelectList(db.ResortLocations, "ResortLocationID", "ResortName", reservation.ResortLocationID);
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReservationID,ResortLocationID,CustomerID,ReservationDate")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.Customers, "ID", "FirstName", reservation.CustomerID);
            ViewBag.ResortLocationID = new SelectList(db.ResortLocations, "ResortLocationID", "ResortName", reservation.ResortLocationID);
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservation reservation = db.Reservations.Find(id);
            db.Reservations.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
