﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReservationResort.Data.EF;
using Microsoft.AspNet.Identity;

namespace ReservationResort.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: Customers
        public ActionResult Index()
        {
            if (User.IsInRole("Admin") || User.IsInRole("employee"))
            {
                return View(db.Customers.ToList());
            }
            else if(User.IsInRole("customer"))
            {
                var c = User.Identity.GetUserId();
                return Redirect("Customers/Details/" + c);
            }
            return View(db.Customers.ToList());

        }

        // GET: Customers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
        [Bind(Include = "ID,FirstName,LastName,PhotoURL,SpecialNotes,IsActive,DateAdded,Phone,Email")]
        Customer customer, HttpPostedFileBase customerPhoto)
        {
            customer.ID = User.Identity.GetUserId();
            customer.IsActive = true;

            if (ModelState.IsValid)
            {

                #region File Upload
                //checking to see if there was a file upload, if not, default to the noimage.png
                string file = "noImage.png";

                if (customerPhoto != null)
                {
                    //getting the name 
                    file = customerPhoto.FileName;

                    //getting the extension
                    string ext = file.Substring(file.LastIndexOf('.'));

                    //checking extensions against a list of 'good' extensions
                    string[] goodExts = { ".jpg", "jpeg", ".png", ".gif" };

                    //seeing if this file is a photo
                    if (goodExts.Contains(ext))
                    {
                        //this is a valid photo
                        //checking the file size to make sure it's not too Large
                        if (customerPhoto.ContentLength <= 10000000)//10mb
                        {
                            //incase this is the second time around and they have a file that fits
                            Session["SizeError"] = null;

                            //creating a unique name for each customer photo
                            file = customer.LastName + DateTime.Now.Year + ext;

                            //save file to server
                            customerPhoto.SaveAs((Server.MapPath("~/Content/Images/" + file)));

                        }//if less than 10 Million bytes
                        else
                        {
                            //the photo was too big
                            Session["SizeError"] = "Your photo was to big, try again";
                            return RedirectToAction("Edit", customer.ID);

                        }
                    }//if contains extension


                    //Updating the PhotoURL Property of new student
                    //in the database
                    customer.PhotoURL = file;


                }//end if customerPhoto not null

                #endregion


                //in case this is the second time around and they have a file that fits
                Session["SizeError"] = null;


                db.Customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Index");

            }

            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,PhotoURL,SpecialNotes,IsActive,DateAdded,Phone,Email")] Customer customer,
             HttpPostedFileBase customerPhoto)
        {
            if (ModelState.IsValid)
            {

                #region File Upload
                //checking to see if there was a file upload, if not, default to the noimage.png
                string file = "noImage.png";

                if (customerPhoto != null)
                {
                    //getting the name 
                    file = customerPhoto.FileName;

                    //getting the extension
                    string ext = file.Substring(file.LastIndexOf('.'));

                    //checking extensions against a list of 'good' extensions
                    string[] goodExts = { ".jpg", "jpeg", ".png", ".gif" };

                    //seeing if this file is a photo
                    if (goodExts.Contains(ext))
                    {
                        //this is a valid photo
                        //checking the file size to make sure it's not too Large
                        if (customerPhoto.ContentLength <= 10000000)//10mb
                        {
                            //incase this is the second time around and they have a file that fits
                            Session["SizeError"] = null;

                            //creating a unique name for each customer photo
                            file = customer.LastName + DateTime.Now.Year + ext;

                            //save file to server
                            customerPhoto.SaveAs((Server.MapPath("~/Content/Images/" + file)));

                        }//if less than 10 Million bytes
                        else
                        {
                            //the photo was too big
                            Session["SizeError"] = "Your photo was to big, try again";
                            return RedirectToAction("Edit", customer.ID);

                        }
                    }//if contains extension


                    //Updating the PhotoURL Property of new student
                    //in the database
                    customer.PhotoURL = file;


                }//end if customerPhoto not null

                #endregion


                //in case this is the second time around and they have a file that fits
                Session["SizeError"] = null;

                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Customer customer = db.Customers.Find(id);
            AspNetUser user = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(user);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
