﻿using System.Web.Mvc;
using System.Net.Mail;
using ReservationResort.Models;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Contact(ContactViewModel contact)
        {
            //check the contact object for validity
            if (ModelState.IsValid)
            {
                //Create a body for the email (words)
                string body = string.Format($"Name: {contact.Name}<br />Email: {contact.Email}" +
                $"<br />Phone: {contact.Phone}<br /><br />{contact.Message}");

                //create and configure the Mail message (letter)
                MailMessage msg = new MailMessage("admin@comfortwalker.net",
                    contact.Email,//this is where we are sending to
                    contact.Phone, //subject of the message
                    body);

                //Configure the mail message object (evelope)
                msg.IsBodyHtml = true; //the body of the message is Html
                                       //msg.CC.Add("comfortwalker@outlook.com"); optional
                                       // msg.Bcc.Add("comfortwalker@outlook.com");optionl
                msg.Priority = MailPriority.High; //we want the email to end up in their mailbox

                //Create and configure the SMTP client (Mail person)
                SmtpClient client = new SmtpClient("mail.comfortwalker.net"); //Mail person
                client.Credentials = new System.Net.NetworkCredential("admin@comfortwalker.net", "Lovecomfort1@"); //stamp
                                                                                                                   //this part just attaches the username and password to the client so we can send the email
                                                                                                                   //client.Port = 8889; //the original is 25, but in case that port is being blocked
                                                                                                                   //you can change the port

                //send the email
                using (client)
                {

                    try
                    {
                        client.Send(msg); //sending the MailMessage object
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was an error sending your message, please email comfortwalker@outlook.com";
                        return View();
                    }

                }//using
                 //send the user to the Contact Confirmation view
                 //pass the contact object with it 

                return View("ContactConfirmation", contact);

            }//if
            else
            {
                return View(); //the model isn't valid, return to form
            }//else

        }

    }
}
